﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    public class WeatherController : Controller
    {
        private HttpClient _client;

        public WeatherController()
        {
            _client = new HttpClient
            {
                BaseAddress = new Uri("https://opendata-download-metfcst.smhi.se"),
                Timeout = new TimeSpan(0, 0, 0, 0, 2000),
            };

            _client.DefaultRequestHeaders.AcceptCharset.Add(new StringWithQualityHeaderValue("utf-8"));
        }

        // GET api/values/5
        [HttpGet("get-weather-forecast")]
        public string GetWeatherForecast(double lon, double lat)
        {
            try
            {
                var uri = $"/api/category/pmp3g/version/2/geotype/point/lon/{lon}/lat/{lat}/data.json";
                return _client.GetStringAsync(uri).Result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
    }
}
