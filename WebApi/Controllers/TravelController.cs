﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    public class TravelController : Controller
    {
        private HttpClient _client;
        private const string _orebroCentralstationId = "740000133";
        private const string _apiKey = "0e8eb279-dd0e-4b30-8f5d-ca0e244f9b6b";

        public TravelController()
        {
            _client = new HttpClient
            {
                BaseAddress = new Uri("https://api.resrobot.se/v2/"),
                Timeout = new TimeSpan(0, 0, 0, 0, 2000),
            };

            _client.DefaultRequestHeaders.AcceptCharset.Add(new StringWithQualityHeaderValue("utf-8"));
        }

        // GET api/values/5
        [HttpGet("get-stations")]
        public string GetStations(string search)
        {
            try
            {
                var uri = $"location.name.json?key={_apiKey}&input={search}";
                return _client.GetStringAsync(uri).Result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }

        [HttpGet("get-travel-by-destination")]
        public string GetTravelByDestination (string destinationId)
        {
            try
            {
                var uri = $"trip?key={_apiKey}&originId={_orebroCentralstationId}&destId={destinationId}&format=json";
                return _client.GetStringAsync(uri).Result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
    }
}
