﻿
function init() {
    initSearchTripEvent();
}

function initSearchTripEvent() {
    $('#location-search').on('keyup', function () {
        $('#no-travels').hide();
        delay(function () {
            $('#travel-information').fadeOut();
            $('#no-travels').fadeOut();

            var searchVal = $('#location-search').val().toLowerCase();
            if (searchVal === '') {
                $('#travel-information').fadeOut();
                return;
            }

            $.ajax({
                url: 'StartPage/GetTrip',
                data: { search: capitalizeFirstLetter(searchVal) },
                method: "POST",
                success: function (response) {
                    if (response.noResults === true) {
                        $('#no-travels').fadeIn();
                    } else {
                        var date = response.trip.Origin.IsToday === true ? "idag" : response.trip.Origin.Date;

                        var cloudyString = response.weather.IsCloudy ? "Övervägande molnigt" : "Övervägande soligt";

                        $('#destination-name').text(response.trip.Destination.Name);
                        $('#departure-time').text(response.trip.Origin.Time);
                        $('#departure-date').text(date);
                        $('#temperature').text(response.weather.DegreesCelsius);
                        $('#cloudiness').text(cloudyString);
                        $('#windspeed').text(response.weather.WindSpeed);

                        $('#travel-information').fadeIn();
                    }
                }
            });
        }, 800);
    });
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

var delay = (function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();


init();

