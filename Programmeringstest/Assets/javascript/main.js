﻿
function init() {
    initAjaxLoadEvent();
}


function initAjaxLoadEvent() {
    $(document).on('ajaxStart', function () {
        $('#fountainG').fadeIn();
    });

    $(document).on('ajaxStop', function () {
        $('#fountainG').hide();
    });
}

init();