﻿using Programmeringstest.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Programmeringstest.Controllers
{
    public class StartPageController : Controller
    {
        private readonly ITravelService _travelService;
        private readonly IWeatherService _weatherService;


        public StartPageController(ITravelService travelService, IWeatherService weatherService)
        {
            _weatherService = weatherService;
            _travelService = travelService;
        }

        public ActionResult Index ()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetTrip (string search)
        {
            var trip = _travelService.GetTrip(search);

            if (trip == null) return Json(new
            {
                noResults = true
            });

            var weather = _weatherService.GetWeatherForecast(trip);

            return Json(new
            {
                trip,
                weather
            });
                                  
        }
    }
}