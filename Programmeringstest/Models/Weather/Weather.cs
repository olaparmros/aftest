﻿using System;
using System.Collections.Generic;

namespace Programmeringstest.Models.Weather
{
    public class Weather
    {
        public DateTime ApprovedTime { get; set; }
        public DateTime ReferenceTime { get; set; }
        public Geometry Geometry { get; set; }
        public List<TimeSery> TimeSeries { get; set; }
    }

    public class Geometry
    {
        public string Type { get; set; }
        public List<List<double>> Coordinates { get; set; }
    }

    public class Parameter
    {
        public string Name { get; set; }
        public string LevelType { get; set; }
        public int Level { get; set; }
        public string Unit { get; set; }
        public List<double> Values { get; set; }
    }

    public class TimeSery
    {
        public DateTime ValidTime { get; set; }
        public List<Parameter> Parameters { get; set; }
    }


}