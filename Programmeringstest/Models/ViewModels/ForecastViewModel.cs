﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Programmeringstest.Models.ViewModels
{
    public class ForecastViewModel
    {
        public int DegreesCelsius { get; set; }

        public int WindSpeed { get; set; }

        public bool IsCloudy { get; set; }
    }
}