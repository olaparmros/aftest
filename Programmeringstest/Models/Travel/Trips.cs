﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Programmeringstest.Models
{
    public class Trips
    {
        public List<Trip> Trip { get; set; }
    }

    public class Trip
    {
        public LegList LegList { get; set; }
    }

    public class LegList
    {
        public List<Leg> Leg { get; set; }
    }

    public class Leg
    {
        public Destination Destination { get; set; }
        public Origin Origin { get; set; }
    }

    public class Destination
    {
        public string Name { get; set; }
        public double Lon { get; set; }
        public double Lat { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }

        public DateTime ArrivalDateTime => DateTime.Parse($"{Date} {Time}");
        public bool IsToday => ArrivalDateTime.Date == DateTime.Today;
    }

    public class Origin
    {
        public string Name { get; set; }
        public double Lon { get; set; }
        public double Lat { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }

        public DateTime DepartureDateTime => DateTime.Parse($"{Date} {Time}");
        public bool IsToday => DepartureDateTime.Date == DateTime.Today;
    }
}