﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Programmeringstest.Models
{
    [Serializable, XmlRoot("LocationList")]
    public class StopLocation
    {
        public string Id { get; set; }
        public string ExtId { get; set; }
        public string Name { get; set; }
        public double Lon { get; set; }
        public double Lat { get; set; }
        public int Weight { get; set; }
        public int Products { get; set; }
    }
}