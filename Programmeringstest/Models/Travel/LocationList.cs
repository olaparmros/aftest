﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace Programmeringstest.Models
{
    public class LocationList
    {
        public List<StopLocation> StopLocation { get; set; }
    }
}