using Programmeringstest.Services;
using Programmeringstest.Services.WebApi;
using System.Web.Mvc;
using Unity;
using Unity.Mvc5;

namespace Programmeringstest
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            container.RegisterType<IWeatherService, WeatherService>();
            container.RegisterType<ITravelService, TravelService>();
            container.RegisterType<IWebApiService, WebApiService>();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}