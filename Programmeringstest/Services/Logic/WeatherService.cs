﻿using Programmeringstest.Models;
using Programmeringstest.Models.ViewModels;
using Programmeringstest.Models.Weather;
using Programmeringstest.Services.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Programmeringstest.Services
{
    public class WeatherService : IWeatherService
    {
        private readonly IWebApiService _webApiService;

        public WeatherService(IWebApiService webApiService)
        {
            _webApiService = webApiService;
        }

        public ForecastViewModel GetWeatherForecast(Leg trip)
        {
            var weather = _webApiService.GetWeatherForecast(trip.Destination.Lon, trip.Destination.Lat);
            var arrivalTime = trip.Destination.ArrivalDateTime;
            var forecastClosestToTravel = weather.TimeSeries.OrderBy(t => Math.Abs((t.ValidTime - arrivalTime).Ticks)).First();
            var degreesCelsius = forecastClosestToTravel.Parameters.SingleOrDefault(x => x.Name == "t").Values.First();
            var isCloudy = forecastClosestToTravel.Parameters.SingleOrDefault(x => x.Name == "tcc_mean").Values.First() > 3;
            var windSpeed = forecastClosestToTravel.Parameters.SingleOrDefault(x => x.Name == "ws").Values.First();

            var viewmodel = new ForecastViewModel
            {
                DegreesCelsius = (int)Math.Floor(degreesCelsius),
                IsCloudy = isCloudy,
                WindSpeed = (int)Math.Floor(windSpeed)
            };


            return viewmodel;
        }
    }
}