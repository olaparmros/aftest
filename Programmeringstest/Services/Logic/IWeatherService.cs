﻿using Programmeringstest.Models;
using Programmeringstest.Models.ViewModels;
using Programmeringstest.Models.Weather;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Programmeringstest.Services
{
    public interface IWeatherService
    {
        ForecastViewModel GetWeatherForecast(Leg trip);
    }
}