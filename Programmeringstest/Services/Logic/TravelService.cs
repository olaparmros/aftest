﻿using Newtonsoft.Json;
using Programmeringstest.Models;
using Programmeringstest.Services.WebApi;
using System.Collections.Generic;
using System.Linq;

namespace Programmeringstest.Services
{
    public class TravelService : ITravelService
    {
        private readonly IWebApiService _webApiService;

        public TravelService(IWebApiService webApiService)
        {
            _webApiService = webApiService;
        }

        public Leg GetTrip (string search)
        {
            var destinations = _webApiService.GetDestinations(search);

            if (destinations == null || !destinations.StopLocation.Any())
            {
                return null;
            }

            var tripsList = new List<Trips>();

            foreach (var destination in destinations.StopLocation)
            {
                var trips = _webApiService.GetTravelsByDestination(destination);
                if (trips != null)
                {
                    tripsList.Add(trips);
                }
            }

            if (!tripsList.Any())
            {
                return null;
            }

            var relevantTrips = tripsList.SelectMany(x => x?.Trip?.SelectMany(y => y?.LegList?.Leg).Where(z => z.Destination.Name.Contains(search))).ToList();

            if (relevantTrips == null || !relevantTrips.Any())
            {
                return null;
            }

            var closestTravel = relevantTrips.OrderBy(x => x.Origin.DepartureDateTime).First();

            return closestTravel;
        }
    }
}