﻿using Programmeringstest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Programmeringstest.Services
{
    public interface ITravelService
    {
        Leg GetTrip(string search);
    }
}