﻿using Newtonsoft.Json;
using Programmeringstest.Models;
using Programmeringstest.Models.Weather;
using Programmeringstest.Static;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace Programmeringstest.Services.WebApi
{
    public class WebApiService : IWebApiService
    {
        private HttpClient _client { get; set; }

        public WebApiService()
        {
            _client = new HttpClient
            {
                BaseAddress = new Uri(Global.API_URL),
            };

            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/plain"));
            _client.DefaultRequestHeaders.AcceptCharset.Add(new StringWithQualityHeaderValue("utf-8"));
        }

        public LocationList GetDestinations(string search)
        {
            var uri = $"Travel/get-stations?search={search}";

            var result = _client.GetStringAsync(uri).Result;

            return result != string.Empty ? JsonConvert.DeserializeObject<LocationList>(result) : null;
        }


        public Trips GetTravelsByDestination(StopLocation destination)
        {
            var trips = new List<Trips>();
            var uri = $"Travel/get-travel-by-destination?destinationId={destination.Id}";
            var result = _client.GetStringAsync(uri).Result;

            return result != string.Empty ? JsonConvert.DeserializeObject<Trips>(result) : null;
        }

        public Weather GetWeatherForecast(double lon, double lat)
        {
            var trips = new List<Trips>();

            var uri = $"Weather/get-weather-forecast?lon={lon}&lat={lat}";

            var result = _client.GetStringAsync(uri).Result;

            return result != string.Empty ? JsonConvert.DeserializeObject<Weather>(result) : null;
        }
    }
}