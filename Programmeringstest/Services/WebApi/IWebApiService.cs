﻿using Programmeringstest.Models;
using Programmeringstest.Models.Weather;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Programmeringstest.Services.WebApi
{
    public interface IWebApiService
    {
        Trips GetTravelsByDestination(StopLocation destination);

        LocationList GetDestinations(string search);

        Weather GetWeatherForecast(double lon, double lat);
    }
}